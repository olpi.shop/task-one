package com.olpi;

public class Calculator {

    public Long addition(String sequence) {

        if (sequence == null) {
            throw new IllegalArgumentException("String is null");
        }
        Long total = 0L;

        for (String number : sequence.split(" ")) {
            if (number.trim().isEmpty()) {
                continue;
            }
            if (isLong(number)) {
                total += Long.parseLong(number);
            } else {
                throw new IllegalArgumentException(
                        "The sequence includes non numeric value!");
            }
        }
        return total;
    }

    public Long multiplication(String sequence) {

        if (sequence == null) {
            throw new IllegalArgumentException("String is null");
        }

        Long product = 1L;
        for (String number : sequence.split(" ")) {
            if (number.trim().isEmpty()) {
                continue;
            }
            if (isLong(number)) {
                product *= Long.parseLong(number);
            } else {
                throw new IllegalArgumentException(
                        "The sequence includes non numeric value!");
            }
        }
        return product;
    }

    private boolean isLong(String text) {
        try {
            Long.parseLong(text);
            return true;
        } catch (NumberFormatException ex) {
            return false;
        }
    }
}
