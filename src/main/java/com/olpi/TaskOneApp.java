package com.olpi;

import java.util.Scanner;

public class TaskOneApp {
    
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        Calculator сalculator = new Calculator();

        System.out.println("Please enter your name:");
        String name = scanner.nextLine();
        System.out.println("Hello " + name + "!\nWelcome to the app!"
                + "\nPlease enter a sequence of numbers:"
                + "\nOr \"exit\" to close the program");

        while (scanner.hasNext()) {
            
            String initialText = scanner.nextLine();
            
            if (initialText.equalsIgnoreCase("exit")) {
                break;
            }
            try {
                System.out.println(
                        "The total is: " + сalculator.addition(initialText));
                System.out.println("The product is: "
                        + сalculator.multiplication(initialText));
                
            } catch (IllegalArgumentException ex) {
                System.out.println(ex.getMessage() + "\nPlease try again.");
            }
            System.out.println(
                    "Enter new sequence or \"exit\" to close the program:");
        }
        scanner.close();
    }

}
